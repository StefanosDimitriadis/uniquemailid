﻿using NLog;
using NLog.Fluent;
using System;

namespace UniqueMailId
{
	/// <summary>
	/// The loggers that will be used in the <see cref="IUniqueMailIdSender"/> utility
	/// </summary>
	internal static class ApplicationLogger
    {
        internal static ILogger System { get; private set; }

        static ApplicationLogger()
        {
            System = LogManager.GetLogger("System");
        }

        internal static void LogInformation(this ILogger @this, string context, object data = null)
        {
            @this.Info()
                .Property("Context", context)
                .Message("{0}", data ?? (object)string.Empty)
                .Write();
        }

        internal static void LogInformation(this ILogger @this, string callId, string context, object data = null)
        {
            @this.Info()
                .Property("CallId", callId)
                .Property("Context", context)
                .Message("{0}", data ?? (object)string.Empty)
                .Write();
        }

        internal static void LogInformation(this ILogger @this, string callId, string context, string elapsedMilliseconds, object data = null)
        {
            @this.Info()
                .Property("CallId", callId)
                .Property("Context", context)
                .Property("Elapsed Time", $"[{elapsedMilliseconds}] ms")
                .Message("{0}", data ?? (object)string.Empty)
                .Write();
        }

        internal static void LogWarning(this ILogger @this, string context, object data = null)
        {
            @this.Warn()
                .Property("Context", context)
                .Message("{0}", data ?? (object)string.Empty)
                .Write();
        }

        internal static void LogException(this ILogger @this, string context, Exception exception, object data = null)
        {
            @this.Error()
                .Exception(exception)
                .Property("Context", context)
                .Message("{0}", data ?? (object)string.Empty)
                .Write();
        }

        internal static void LogException(this ILogger @this, string callId, string context, Exception exception, object data = null)
        {
            @this.Error()
                .Exception(exception)
                .Property("CallId", callId)
                .Property("Context", context)
                .Message("{0}", data ?? (object)string.Empty)
                .Write();
        }
    }
}
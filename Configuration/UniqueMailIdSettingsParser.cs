﻿using Common.Utilities.LogUtility;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Runtime.CompilerServices;

namespace UniqueMailId.Configuration
{
	/// <summary>
	/// The settings that will be used in the <see cref="IUniqueMailIdSender"/> utility
	/// </summary>
	internal class UniqueMailIdSettingsParser
	{
		private static readonly string classContext = $"{nameof(UniqueMailIdSettingsParser)}";

		internal UniqueMailIdSettings UniqueMailId { get; private set; }

		#region Singleton
		private static readonly Lazy<UniqueMailIdSettingsParser> _Instance = new Lazy<UniqueMailIdSettingsParser>(() => new UniqueMailIdSettingsParser());

		private UniqueMailIdSettingsParser()
		{
			Initialize();
		}

		public static UniqueMailIdSettingsParser Instance { get { return _Instance.Value; } }
		#endregion

		private void Initialize()
		{
			LoadSettings();
		}

		/// <summary>
		/// Loads the settings from the json file
		/// </summary>
		private void LoadSettings()
		{
			IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
				.AddJsonFile(source =>
				{
					source.Optional = false;
					source.Path = "./Configuration/UniqueMailIdSettings.json";
					source.ReloadDelay = 1000;
					source.ReloadOnChange = true;
				});

			IConfigurationRoot configurationRoot = configurationBuilder.Build();

			BindSettings(configurationRoot);

			ChangeToken.OnChange(
				() => configurationRoot.GetReloadToken(),
				() => BindSettings(configurationRoot, logChange: true));
		}

		/// <summary>
		/// Binds the settings from the json file to the <see cref="UniqueMailIdSettings"/> instance
		/// </summary>
		/// <param name="configurationRoot"></param>
		/// <param name="logChange">Defines if the changes should be logged</param>
		private void BindSettings(IConfigurationRoot configurationRoot, bool logChange = false)
		{
			string methodContext = GetMethodContext();

			Settings settings = configurationRoot.Get<Settings>();

			if (settings == null)
				throw new Exception($"Smtp client settings are null");

			if (logChange)
				ApplicationLogger.SystemInformation.LogWithContext(
					context: $"{methodContext}",
					data: new
					{
						Message = $"Settings changed",
						OldSettings = JsonConvert.SerializeObject(Instance.UniqueMailId),
						NewSettings = JsonConvert.SerializeObject(settings.UniqueMailId)
					});

			UniqueMailId = settings.UniqueMailId;
		}

		/// <summary>
		/// Returns the context name
		/// </summary>
		/// <param name="memberName">The name of the method</param>
		private string GetMethodContext([CallerMemberName] string memberName = "")
		{
			string methodContext = $"{classContext}.{memberName}";

			return methodContext;
		}

		/// <summary>
		/// Copy of <see cref="UniqueMailIdSettings"/> to (custom) bind settings from the json file to the <see cref="UniqueMailIdSettings"/> instance
		/// </summary>
		private class Settings
		{
			/// <summary>
			/// The <see cref="IUniqueMailIdSender"/> settings
			/// </summary>
			public UniqueMailIdSettings UniqueMailId { get; set; }
		}
	}

	/// <summary>
	/// The settings of the <see cref="IUniqueMailIdSender"/> utility
	/// </summary>
	internal class UniqueMailIdSettings
	{
		/// <summary>
		/// The mode of the data
		/// </summary>
		public string DataMode { get; set; }
		/// <summary>
		/// The name of the file containing the current unique mail id
		/// </summary>
		public string UniqueMailIdFileName { get; set; }
		/// <summary>
		/// The mail address to send the mail to
		/// </summary>
		public string ToMailAddress { get; set; }
		/// <summary>
		/// The subject of the mail to be sent
		/// </summary>
		public string Subject { get; set; }
	}
}
using Common.Utilities;
using Common.Utilities.MailSender.Types;
using System.Utilities;
using UniqueMailId.Configuration;

namespace UniqueMailId
{
	/// <summary>
	/// The contract of the <see cref="IUniqueMailIdSender"/> utility
	/// </summary>
	public interface IUniqueMailIdSender
	{
		/// <summary>
		/// Sends the unique mail id
		/// </summary>
		void SendUniqueMailId();
	}

	/// <summary>
	/// The implementation of the <see cref="IUniqueMailIdSender"/>
	/// </summary>
	internal class UniqueMailIdSender : IUniqueMailIdSender
	{
		private static readonly IMailSenderUtility MailSenderUtility = DependencyFactory.Resolve<IMailSenderUtility>();
		private static readonly Data.IContext Context = DependencyFactory.Resolve<Data.IContext>();

		/// <summary>
		/// Sends the unique mail id
		/// </summary>
		public void SendUniqueMailId()
		{
			int currentId = Context.GetCurrentId();

			int newId = UpdateId(currentId);

			SendMail(newId);

			Context.ReplaceCurrentIdWithNewId(newId);
		}

		/// <summary>
		/// Increments the current id
		/// </summary>
		/// <param name="currentId">The current id</param>
		private int UpdateId(int currentId)
		{
			int newId = ++currentId;

			return newId;
		}

		/// <summary>
		/// Sends the mail
		/// </summary>
		/// <param name="newId"></param>
		private void SendMail(int newId)
		{
			MailInformation mailInformation = CreateMailInformation(newId);

			MailSenderUtility.SendMail(mailInformation);
		}

		/// <summary>
		/// Creates and returns the <see cref="MailInformation"/>
		/// </summary>
		/// <param name="newId">The new unique mail id</param>
		private MailInformation CreateMailInformation(int newId)
		{
			MailInformation mailInformation = new MailInformation
			{
				Body = $"The newest Id is {newId}.",
				Subject = $"{UniqueMailIdSettingsParser.Instance.UniqueMailId.Subject}",
				BodyIsHTML = false,
				RecipientAddress = $"{UniqueMailIdSettingsParser.Instance.UniqueMailId.ToMailAddress}",
				RecipientDisplayName = string.Empty
			};

			return mailInformation;
		}
	}
}
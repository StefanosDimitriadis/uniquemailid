﻿namespace UniqueMailId.Data
{
	/// <summary>
	/// The contract of the <see cref="IContext"/>
	/// </summary>
	public interface IContext
	{
		/// <summary>
		/// Reads the current id from the file
		/// </summary>
		int GetCurrentId();
		/// <summary>
		/// Replaces the value of the current id with the new id in the file
		/// </summary>
		/// <param name="newId">The new id to replace the current one</param>
		void ReplaceCurrentIdWithNewId(int newId);
	}
}
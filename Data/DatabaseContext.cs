﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.SQLite;
using System.Linq;
using UniqueMailId.Configuration;

namespace UniqueMailId.Data
{
	/// <summary>
	/// The contract of the <see cref="IDatabaseContext"/>
	/// </summary>
	public interface IDatabaseContext
	{
		DbSet<MailId> MailIds { get; set; }
	}

	/// <summary>
	/// The database implementation of the <see cref="IContext"/> contract
	/// </summary>
	public class DatabaseContext : DbContext, IDatabaseContext, IContext
	{
		public static string FileName { get; set; } = $"{UniqueMailIdSettingsParser.Instance.UniqueMailId.UniqueMailIdFileName}";

		private static readonly SQLiteConnectionStringBuilder ConnectionStringBuilder = new SQLiteConnectionStringBuilder
		{
			DataSource = FileName
		};

		private static readonly string ConnectionString = ConnectionStringBuilder.ConnectionString;

		private static readonly SQLiteConnection Connection = new SQLiteConnection()
		{
			ConnectionString = ConnectionString
		};

		public DatabaseContext() :
			base(existingConnection: Connection, contextOwnsConnection: true)
		{ }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}

		/// <summary>
		/// Reads the current id from the file
		/// </summary>
		public int GetCurrentId()
		{
			int currentId = MailIds.Max(mailId => mailId.CurrentId);

			return currentId;
		}

		/// <summary>
		/// Replaces the value of the current id with the new id
		/// </summary>
		/// <param name="newId">The new id to replace the current one</param>
		public void ReplaceCurrentIdWithNewId(int newId)
		{
			MailId mailId = new MailId();

			MailIds.Add(mailId);

			SaveChanges();
		}

		/// <summary>
		/// The ids of the mails
		/// </summary>
		public DbSet<MailId> MailIds { get; set; }
	}

	/// <summary>
	/// Represents the MailId table of the database
	/// </summary>
	public class MailId
	{
		/// <summary>
		/// The current id
		/// </summary>
		[Key]
		public int CurrentId { get; set; }
	}
}
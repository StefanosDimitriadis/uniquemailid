﻿using Common.Utilities;
using System;
using System.Linq;
using System.Reflection;
using System.Utilities;
using UniqueMailId.Configuration;

namespace UniqueMailId.Data
{
	/// <summary>
	/// The implementation of the <see cref="IContext"/> contract
	/// </summary>
	public class TextFileContext : IContext
	{
		private IFileHandlerUtility _FileHandlerUtility;

		public TextFileContext()
			: this(DependencyFactory.Resolve<IFileHandlerUtility>()) { }

		internal TextFileContext(IFileHandlerUtility fileHandlerUtility)
		{
			_FileHandlerUtility = fileHandlerUtility;
		}

		public string FileName { get; set; } = $"{UniqueMailIdSettingsParser.Instance.UniqueMailId.UniqueMailIdFileName}";

		public string ClassName
		{
			get
			{
				return $"{nameof(TextFileContext)}";
			}
		}

		public AssemblyName AssemblyName
		{
			get
			{
				return typeof(TextFileContext).Assembly.GetName();
			}
		}

		/// <summary>
		/// Reads the current id from the file
		/// </summary>
		public int GetCurrentId()
		{
			string currentId = _FileHandlerUtility
				.ReadLines(FileName)
				.ToList()
				.FirstOrDefault();

			ParseId(currentId, out int currentIdAsInt);

			return currentIdAsInt;
		}

		/// <summary>
		/// Tries to convert the current id from string to int
		/// </summary>
		/// <param name="currentIdAsString">The current id as string</param>
		/// <param name="currentIdAsInt">The current id as int</param>
		private void ParseId(string currentIdAsString, out int currentIdAsInt)
		{
			if (int.TryParse(currentIdAsString, out currentIdAsInt) == false)
				throw new Exception($"Current id [{currentIdAsString}] is not a number!");
		}

		/// <summary>
		/// Replaces the value of the current id with the new id in the file
		/// </summary>
		/// <param name="newId">The new id to replace the current one</param>
		public void ReplaceCurrentIdWithNewId(int newId)
		{
			string newIdAsString = newId.ToString();

			_FileHandlerUtility.WriteAllText(
				path: FileName,
				contents: newIdAsString);
		}
	}
}
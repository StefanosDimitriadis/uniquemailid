﻿//using Common.Utilities.LoggingAdviceUtility;
//using Spring.Aop.Framework;
using Spring.Context;
using Spring.Context.Support;
using System;

namespace UniqueMailId
{
	class Program
	{
		#region Using xml file for setting Spring AOP configuration
		static void Main(string[] args)
		{
			try
			{
				Startup.Initialize();

				IUniqueMailIdSender uniqueMailIdSender = CreateUniqueMailIdSender();

				uniqueMailIdSender.SendUniqueMailId();

				Console.ForegroundColor = ConsoleColor.Green;

				Console.WriteLine("Mail sent successfully");
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception);

				Console.ForegroundColor = ConsoleColor.Red;

				Console.WriteLine("Mail was not sent");
			}
			finally
			{
				Console.ReadLine();
			}
		}

		/// <summary>
		/// Creates and returns a <see cref="IUniqueMailIdSender"/>
		/// </summary>
		private static IUniqueMailIdSender CreateUniqueMailIdSender()
		{
			IApplicationContext applicationContext = GetApplicationContext();

			IUniqueMailIdSender uniqueMailIdSender = GetUniqueMailIdSender(applicationContext);

			return uniqueMailIdSender;
		}

		/// <summary>
		/// Gets the <see cref="IApplicationContext"/>
		/// </summary>
		private static IApplicationContext GetApplicationContext()
		{
			IApplicationContext applicationContext = ContextRegistry.GetContext();

			return applicationContext;
		}

		/// <summary>
		/// Gets the <see cref="IUniqueMailIdSender"/>
		/// </summary>
		/// <param name="applicationContext"></param>
		private static IUniqueMailIdSender GetUniqueMailIdSender(IApplicationContext applicationContext)
		{
			IUniqueMailIdSender uniqueMailIdSender = applicationContext.GetObject<IUniqueMailIdSender>("UniqueMailIdSender");

			return uniqueMailIdSender;
		}
		#endregion Using xml file for setting Spring AOP configuration

		#region Programmatically setting up Spring configuration
		//static void Main(string[] args)
		//{
		//	try
		//	{
		//		IUniqueMailIdSender uniqueMailIdSender = CreateUniqueMailIdSender();

		//		ProxyFactory proxyFactory = CreateProxyFactory(uniqueMailIdSender);

		//		LoggingAdvice loggingAdvice = CreateLoggingAdvice();

		//		AddLoggingAdvice(proxyFactory, loggingAdvice);

		//		uniqueMailIdSender = proxyFactory.GetProxy() as IUniqueMailIdSender;

		//		uniqueMailIdSender.SendUniqueMailId();

		//		Console.WriteLine("Mail sent successfully");
		//	}
		//	catch (Exception exception)
		//	{
		//		Console.WriteLine(exception);

		//Console.WriteLine("Mail was not sent");
		//	}
		//	finally
		//	{
		//		Console.ReadLine();
		//	}
		//}

		//private static IUniqueMailIdSender CreateUniqueMailIdSender()
		//{
		//	IUniqueMailIdSender uniqueMailIdSender = UniqueMailIdSender.Instance;

		//	return uniqueMailIdSender;
		//}

		//private static ProxyFactory CreateProxyFactory(IUniqueMailIdSender uniqueMailIdSender)
		//{
		//	ProxyFactory proxyFactory = new ProxyFactory(uniqueMailIdSender);

		//	return proxyFactory;
		//}

		//private static LoggingAdvice CreateLoggingAdvice()
		//{
		//	LoggingAdvice loggingAdvice = new LoggingAdvice();

		//	return loggingAdvice;
		//}

		//private static void AddLoggingAdvice(ProxyFactory proxyFactory, LoggingAdvice loggingAdvice)
		//{
		//	proxyFactory.AddAdvice(loggingAdvice);
		//}
		#endregion Programmatically setting up Spring configuration
	}
}
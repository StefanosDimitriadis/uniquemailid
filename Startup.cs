﻿using Common.Utilities;
using System.Reflection;
using System.Utilities;
using UniqueMailId.Configuration;
using Unity;

namespace UniqueMailId
{
	public static class Startup
	{
		private static IInput _Input = new Input();
		private static readonly IFactoryUtility _FactoryUtility = new FactoryUtility();

		public static void Initialize()
		{
			CreateContext();

			CreateMailSenderUtility();
		}

		/// <summary>
		/// Creates a <see cref="IContext"/>
		/// </summary>
		private static void CreateContext()
		{
			AssemblyName assemblyName = Assembly.GetExecutingAssembly().GetName();

			_Input.SetAssemblyName(assemblyName);

			string className = $"{assemblyName.Name}.{UniqueMailIdSettingsParser.Instance.UniqueMailId.DataMode}";

			_Input.SetClassName(className);

			Data.IContext context = _FactoryUtility.Build<Data.IContext>(_Input);

			DependencyFactory.Container.RegisterInstance<Data.IContext>(context);
		}

		/// <summary>
		/// Creates a <see cref="IMailSenderUtility"/>
		/// </summary>
		private static void CreateMailSenderUtility()
		{
			AssemblyName assemblyName = Assembly.GetAssembly(typeof(MailSenderUtility)).GetName();

			_Input.SetAssemblyName(assemblyName);

			string className = $"{assemblyName.Name}.{nameof(MailSenderUtility)}";

			_Input.SetClassName(className);

			IMailSenderUtility mailSenderUtility = _FactoryUtility.Build<IMailSenderUtility>(_Input);

			DependencyFactory.Container.RegisterInstance<IMailSenderUtility>(mailSenderUtility);
		}
	}

	/// <summary>
	/// The implementation of the <see cref="IInput"/> contract
	/// </summary>
	internal class Input : IInput
	{
		public string ClassName { get; private set; }

		public AssemblyName AssemblyName { get; private set; }

		/// <summary>
		/// Sets the name of the assembly
		/// </summary>
		/// <param name="assemblyName">The assembly name to use</param>
		public void SetAssemblyName(AssemblyName assemblyName)
		{
			AssemblyName = assemblyName;
		}

		/// <summary>
		/// Sets the name of the class
		/// </summary>
		/// <param name="className">The class name to use</param>
		public void SetClassName(string className)
		{
			ClassName = className;
		}
	}
}